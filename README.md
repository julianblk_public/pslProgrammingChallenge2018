# PSL Programming Challenge: Números Ordenados

## Solución O(N)

How to start
---

1. Run `git clone https://gitlab.com/julianblk_public/pslProgrammingChallenge2018.git`
1. cd to `pslProgrammingChallenge2018`
1. Replace the file `entrada.txt`
1. Run `javac NumerosOrdenados.java`
1. Run `java NumerosOrdenados`
1. The file `salida.txt` will be created in the same folder with the corresponding output.