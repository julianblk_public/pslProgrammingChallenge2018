import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintStream;
import java.time.Duration;
import java.time.Instant;

public class NumerosOrdenados {
    
    private static String INPUT_FILE = "entrada.txt";
//    private static String INPUT_FILE = "entradaAuto.txt";
    private static String OUTPUT_FILE = "salida.txt";

    public static void main(String[] args) {
        
//        Instant start = Instant.now();
        
        try (   BufferedReader br = new BufferedReader(new FileReader(INPUT_FILE));
                PrintStream ps = new PrintStream(new File(OUTPUT_FILE))) {

            int numCases = Integer.valueOf(br.readLine());
            int i = 0;

            while (true) {
                String line = br.readLine();
                ps.printf("Caso %d: N=%s, O=%s", ++i, line, solve(line));

                if (i == numCases) {
                    break;
                }
                
                ps.println();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

//        Instant end = Instant.now();
//        System.out.println(Duration.between(start, end));
        
    }

    private static StringBuilder solve(String numberIn) {
        
        final StringBuilder numberOut = new StringBuilder();
        final int numLength = numberIn.length();
        int idxStartNineFill = numLength;
        int offset = 0;
        
        for (int i = numLength-2; i > -1; i--) {

            if (numberIn.charAt(i) > numberIn.charAt(i+1) - offset) {
                idxStartNineFill = i+1;
                offset = 1;
            }
            else {
                offset = 0;
            }
        }
        
        for (int i = 0; i < idxStartNineFill-1; i++) {
            if (numberIn.charAt(i) == '0') {
                continue;
            }
            numberOut.append(numberIn.charAt(i));
        }
        
        if (idxStartNineFill == numLength) {
            numberOut.append(numberIn.charAt(idxStartNineFill-1));
            return numberOut;
        }

        if (numberIn.charAt(idxStartNineFill-1) != '1') {
            numberOut.append(Integer.valueOf(numberIn.charAt(idxStartNineFill-1)+"") - 1);
        }
        
        for (int i = idxStartNineFill; i < numLength; i++) {
            numberOut.append("9");
        }
        
        return numberOut;
    }
}
